# -*- coding: utf-8 -*-

import kmlclean2


def clean_all_files():
	for filename in ['ptaki.kml', 'polska.kml', 'reszta-swiata.kml', 'polska-nie-gory.kml']:
		clean_file(filename)


def clean_file(filename):
    print "Reading %s..." % filename
    i = open(filename, 'r').read(999999999)
    print "Sending %s bytes..." % len(i)
    o = kmlclean2.clean_string(kmlclean2.clean_string(i))
    print "Saving received %s bytes." % len(o)
    open(filename, 'w').write(o)

clean_all_files()
