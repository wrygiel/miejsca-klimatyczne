# -*- coding: utf-8 -*-

# This script "cleans up" KML files exported from Google Maps.
# Such "clean" files are much more diff-able and/or mergable.

# Wojciech Rygielski <rygielski@mimuw.edu.pl> -- December 2012.

import sys
import copy

from lxml import etree


def clean_string(s):
    parser = etree.XMLParser(remove_blank_text=True)
    ikml = etree.XML(s, parser)
    okml = etree.XML('<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.2"><Document/></kml>', parser)
    idoc = ikml.find('.//{http://earth.google.com/kml/2.2}Document')
    odoc = okml.find('.//{http://earth.google.com/kml/2.2}Document')
    odoc.append(idoc.find('./{http://earth.google.com/kml/2.2}name'))
    odoc.append(idoc.find('./{http://earth.google.com/kml/2.2}description'))
    iplacemarks = idoc.findall('./{http://earth.google.com/kml/2.2}Placemark')
    for ip in iplacemarks:
        snippet = ip.find('./{http://earth.google.com/kml/2.2}Snippet')
        if snippet is not None:
            ip.remove(snippet)
        styleUrl = ip.find('./{http://earth.google.com/kml/2.2}styleUrl')
        if styleUrl is not None:
            style_id = styleUrl.text[1:]
            ip.remove(styleUrl)
            style = idoc.find('./{http://earth.google.com/kml/2.2}Style[@id="' + style_id + '"]')
            del style.attrib['id']
            ip.append(style)
        odoc.append(ip)
    return etree.tostring(okml, encoding='utf-8', pretty_print=True)


if __name__ == "__main__":
    # Double pass. Intentional.
    print clean_string(clean_string(sys.stdin.read()))
